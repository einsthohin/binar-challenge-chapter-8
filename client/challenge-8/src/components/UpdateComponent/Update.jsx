import React, { Component } from 'react';
import './Update.css';


class Update extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            password: '',
            email: '',
            experience: '0',
            level: 'easy'
        }
    }

    handleUsernameChange = (event) => {
        this.setState({
            username: event.target.value
        })
    }

    handlePasswordChange = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    handleEmailChange = (event) => {
        this.setState({
            email: event.target.value
        })
    }

    handleExperienceChange = (event) => {
        this.setState({
            experience: event.target.value
        })
    }

    handleLevelChange = (event) => {
        this.setState({
            level: event.target.value
        })
    }
    
    handleSubmit = event => {
        alert(`${this.state.username} ${this.state.email} ${this.state.experience} ${this.state.level}`)
    }

    render () {
        return(
            <form onSubmit={this.handleSubmit}>
                <div className="container-update">
                    <h1>Update Player</h1>
                    <label>Username: </label>
                    <input type="text" name="username" value={this.state.username} onChange={this.handleUsernameChange} />
                    <br/><br/>
                    <label>Password: </label>
                    <input type="password" name="password" value={this.state.password} onChange={this.handlePasswordChange} />
                    <br/><br/>
                    <label>Email: </label>
                    <input type="text" name="email" value={this.state.email} onChange={this.handleEmailChange} />
                    <br/><br/>
                    <label>Experience: </label>
                    <select value={this.state.experience} onChange={this.handleExperienceChange}>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                    </select>
                    <br/><br/>
                    <label>Level: </label>
                    <select value={this.state.level} onChange={this.handleLevelChange}>
                        <option value="easy">Easy</option>
                        <option value="medium">Medium</option>
                        <option value="hard">Hard</option>
                    </select>
                    <br/><br/>
                    <button type="submit" className="btn" name="Create">Submit</button>
                </div>
            </form>
        )
    }
};


export default Update;