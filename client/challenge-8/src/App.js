import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Search from './components/SearchComponent/Search';
import Create from './components/CreateComponent/Create';
import Update from './components/UpdateComponent/Update';

function App() {
  return (
    <Router>
      <div>
        <ul>
          <li>
            <Link to="/create">Create Player</Link>
          </li>
          <li>
            <Link to="/search">Search Player</Link>
          </li>
          <li>
            <Link to="/update">Update Player</Link>
          </li>
        </ul>

        <Fragment>
          <Route path="/create" component={Create} />
          <Route path="/search" component={Search} />
          <Route path="/update" component={Update} />
        </Fragment>
      </div>
    </Router>
  );
}

export default App;
